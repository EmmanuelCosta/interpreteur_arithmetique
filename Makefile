CC=g++
CFLAGS=-W -Wall
LDFLAGS=
EXEC=interpreteur
SRC= $(wildcard src/*.cpp)
INC= include/$(wildcard *.h)
OBJ= $(SRC:.cpp=.o)
INCLUDE= ./include/
SOURCE=./src/

interpreteur: $(OBJ)
	@$(CC) -o $@ $^ $(LDFLAGS) -I $(INCLUDE)

main.o: $(INC)

%.o: $(SOURCE)%.cpp
	@$(CC) -o $@ -c $< $(CFLAGS) -I $(INCLUDE)

clean:
	rm -rf $(SOURCE)*.o

mrproper: clean
	rm -rf $(EXEC)

			
