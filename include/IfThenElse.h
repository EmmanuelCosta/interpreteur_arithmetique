/* 
 * File:   IfThenElse.h
 * Author: emmanuel
 *
 * Created on January 23, 2015, 12:18 AM
 */

#ifndef IFTHENELSE_H
#define	IFTHENELSE_H

#include "Expression.h"
#include "../include/Constante.h"


class IfThenElse : public Expression {
public:
    IfThenElse();
    IfThenElse(Expression *condition, Expression *sValide, Expression *sMauvaise);
    IfThenElse(const IfThenElse& orig);
    virtual ~IfThenElse();

    virtual void afficher(ostream& os) const;
    virtual double eval()const;
    virtual Expression* clone() const;
    virtual Expression* getTest() const;
    virtual Expression* getOutTrue() const;
    virtual Expression* getOutFalse() const;

    virtual bool isFunctionof(string x)const;

    virtual Expression* doDerivate(string x)const;
    virtual Expression* simplify();



private:
    Expression* _condition;
    Expression* _sValide;
    Expression* _sMauvaise;
};

#endif	/* IFTHENELSE_H */

