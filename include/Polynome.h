/* 
 * File:   Polynome.h
 * Author: emmanuel
 *
 * Created on February 7, 2015, 1:27 PM
 */

#ifndef POLYNOME_H
#define	POLYNOME_H

#include <map>
#include <set>
#include <cstdarg>
#include "Expression.h"


#include "../include/Constante.h"
#include "../include/Somme.h"
#include "../include/Difference.h"
#include "../include/Produit.h"

class Monome;

class Polynome : public Expression {
public:

    Polynome(string x);
    string getVariableName()const;
    void setVariableName(string x);
    Polynome(const Polynome& orig);

    void addMonome(Monome*monome);

    virtual double eval()const;

    virtual Polynome* clone() const;

    virtual bool isFunctionof(string x)const;

    virtual Expression* simplify();

    virtual Expression* doDerivate(string x)const;






    virtual ~Polynome();
    Polynome operator+(const Polynome&)const;
    Polynome operator-(const Polynome&)const;
    Polynome operator*(const Polynome&)const;
    Polynome operator=(const Polynome&);
    Polynome operator+=(const Polynome&);
    Polynome operator-=(const Polynome&);
    Polynome operator*=(const Polynome&);
    bool operator==(const Polynome&);
    bool operator!=(const Polynome&);
    Expression * doHorner()const;

    friend ostream & operator<<(ostream & os, const Polynome & x);



    virtual void afficher(ostream& os) const;

protected:
    Expression * doHornerFrom(int i)const;

private:
    map<int, Monome*> _polynome;
    set<int> _available;
    string _variable;
    int _degreMax;

    ;

};

#endif	/* POLYNOME_H */

