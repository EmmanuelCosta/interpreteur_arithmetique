/* 
 * File:   Conditionnel.h
 * Author: emmanuel
 *
 * Created on January 23, 2015, 12:06 AM
 */



#ifndef CONDITIONNEL_H
#define	CONDITIONNEL_H


#include "Expression_Binaire.h"
#include "../include/Constante.h"

class Conditionnel: public Expression {
public:
    Conditionnel();
    Conditionnel(Expression *condition, Expression *sValide, Expression *sMauvaise);
    Conditionnel(const Conditionnel& orig);
    
    virtual void afficher(ostream& os) const;
    virtual ~Conditionnel();

    virtual double eval()const;


    virtual Expression* doDerivate(string x)const;

    virtual bool isFunctionof(string x)const;

    virtual Expression* simplify();

    virtual Expression* clone() const;

    
    
    virtual Expression* getTest() const;
    virtual Expression* getOutTrue() const;
    virtual Expression* getOutFalse() const;
    
private:
    Expression* _condition;
    Expression* _sValide;
    Expression* _sMauvaise;
};

#endif	/* CONDITIONNEL_H */

