/* 
 * File:   Pour.h
 * Author: sybille
 *
 * Created on 3 février 2015, 19:02
 */

#ifndef POUR_H
#define	POUR_H

#include "Expression.h"
#include "../include/Constante.h"

class Pour : public Expression {
public:
    Pour();
    Pour(Expression* init, Expression* condition, Expression* actionFinDeBoucle, Expression* calcul);
    Pour(const Pour& origin);
    virtual ~Pour();
    virtual double eval()const;
    virtual Expression* clone() const;
    virtual void afficher(ostream& os) const;
    
    virtual Expression* getInit() const;
    virtual Expression* getCondition() const;
    virtual Expression* getActionFinBoucle() const;
    virtual Expression* getCalcul() const;

    virtual Expression* doDerivate(string x)const;
    virtual Expression* simplify();
    virtual bool isFunctionof(string x)const;


private:
    Expression* _init;
    Expression* _condtion;
    Expression* _actionFinDeBoucle;
    Expression* _calcul;
    

};

#endif	/* POUR_H */

