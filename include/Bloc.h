/* 
 * File:   Bloc.h
 * Author: emmanuel
 *
 * Created on January 23, 2015, 12:08 AM
 */

#ifndef BLOC_H
#define	BLOC_H

#include <list>
#include "Expression.h"
#include "Constante.h"

class Bloc : public Expression {
public:
    Bloc();
    Bloc(const Bloc& orig);
    Bloc(string name, Expression* exp);
    virtual ~Bloc();
    virtual void add(Expression* exp);
    virtual double eval()const;
    virtual Expression* clone() const;


    virtual void afficher(ostream& os)const;
    virtual void afficherTout();

    virtual list<Expression*> getList()const;
    
    virtual Expression* doDerivate(string x)const;

    virtual bool isFunctionof(string x)const;
    virtual Expression* simplify();


private:
    list<Expression*> _bloc;
    string _name;
};

#endif	/* BLOC_H */

