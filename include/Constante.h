/* 
 * File:   Constante.h
 * Author: emmanuel
 *
 * Created on January 22, 2015, 11:56 PM
 */

#ifndef CONSTANTE_H
#define	CONSTANTE_H

#include "Expression.h"

class Constante : public Expression {
public:


    Constante(double val);
    Constante(Expression* exp);
    Constante(const Constante& orig);

    

    virtual void afficher(ostream& os) const;


    virtual Expression* clone() const;

    virtual ~Constante();

    virtual Expression* doDerivate(string x)const;

    virtual bool isFunctionof(string x)const;
    virtual Expression* simplify();



    virtual double eval()const;

    friend ostream & operator<<(ostream & os, const Constante & x);
    friend istream & operator>>(istream & is, Constante & x);
    
//    bool operator<(  Constante &x) ;

private:
    bool _isValueSet;
    double _val;
    Expression* _exp;

};

#endif	/* CONSTANTE_H */

