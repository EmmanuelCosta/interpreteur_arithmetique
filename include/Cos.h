/* 
 * File:   Cos.h
 * Author: emmanuel
 *
 * Created on January 23, 2015, 12:13 AM
 */

#ifndef COS_H
#define	COS_H

#include <math.h>
#include "Expression.h"
#include "../include/Sin.h"
#include "../include/Constante.h"
#include "../include/Produit.h"
#include "../include/Negation.h"
#include "../include/Variable.h"

class Cos:public Expression {
public:


    Cos(double val);
    Cos(Expression* exp);
    Cos(const Cos& orig);


    virtual void afficher(ostream& os) const;

    virtual Expression* clone() const;

    virtual ~Cos();
    virtual Expression* simplify();

    virtual Expression* doDerivate(string x)const;

    virtual bool isFunctionof(string x)const;




    virtual double eval()const;
   
    friend ostream & operator<<(ostream & os, const Cos & x);
    friend istream & operator>>(istream & is, Cos & x);

private:
    bool _isValueSet;
    double _val;
    Expression* _exp;

};

#endif	/* COS_H */

