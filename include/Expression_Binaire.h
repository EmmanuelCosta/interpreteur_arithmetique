/* 
 * File:   Expression_Binaire.h
 * Author: emmanuel
 *
 * Created on January 23, 2015, 12:03 AM
 */

#ifndef EXPRESSION_BINAIRE_H
#define	EXPRESSION_BINAIRE_H

#include "Expression.h"

class Expression_Binaire : public Expression {
protected:
    Expression_Binaire();
    Expression_Binaire(Expression *left, Expression *right, string operateur);
    Expression_Binaire(const Expression_Binaire& orig);
    virtual string getOperateur() const;

    virtual ~Expression_Binaire();


    virtual void afficher(ostream& os) const;

    virtual bool isFunctionof(string x)const;


public:
    //recupere l'operande gauche de l'expression
    Expression* getLeftOperande() const;
    //recupere l'operande droite de l'expression
    Expression* getRightOperande() const;




protected:
    // afin d'eviter qu'une somme soit redefini en une comparaison par exemple
    //permettra de setter l'operateur dans les fils

    Expression* _left;
    Expression* _right;
    string _operateur;


};

#endif	/* EXPRESSION_BINAIRE_H */

