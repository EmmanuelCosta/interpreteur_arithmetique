/* 
 * File:   Soustraction.h
 * Author: emmanuel
 *
 * Created on January 28, 2015, 5:38 PM
 */

#ifndef SOUSTRACTION_H
#define	SOUSTRACTION_H



#include "Expression_Binaire.h"
#include "../include/Variable.h"
#include "../include/Constante.h"

class Difference : public Expression_Binaire {
public:

    Difference(Expression* left, Expression* right);
    Difference(const Difference& orig);
    virtual ~Difference();

    virtual Expression* clone() const;

    virtual Expression* doDerivate(string x)const;
    virtual Expression* simplify();

   


    friend ostream & operator<<(ostream & os, const Difference & x);
    friend istream & operator>>(istream & is, Difference & x);


    virtual void afficher(ostream& os) const;

    virtual double eval()const;


};

#endif	/* SOUSTRACTION_H */

