/* 
 * File:   Variable.h
 * Author: emmanuel
 *
 * Created on January 23, 2015, 12:00 AM
 */

#ifndef VARIABLE_H
#define	VARIABLE_H
#include <map>

#include "Expression.h"
#include "../include/Constante.h"

class Variable : public Expression {
public:
    Variable(string name, Expression *exp);
    Variable(string name);
    Variable(string name, double value);
    Variable(const Variable& orig);
    virtual ~Variable();

    virtual void afficher(ostream& os) const;

    virtual Variable* clone() const;
    static void toutAfficher();
    virtual double eval()const;

    virtual Expression* doDerivate(string x)const;


    virtual bool isFunctionof(string x)const;

    virtual Expression* simplify();

    virtual string getName()const;


    static bool checkIfExpressionExist(string name);
    static void effacerMemoire();
    void set(double val);
    void set(Expression * exp);
    friend ostream & operator<<(ostream & os, const Variable & x);
    friend istream & operator>>(istream & is, Variable & x);



private:
    static map<string, double> _variablePool;
    string _name;

    static void insertToVariablePool(string name, double val);

    static void retirerExpression(string name);

};

#endif	/* VARIABLE_H */

