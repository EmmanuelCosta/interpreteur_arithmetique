/* 
 * File:   Expression.h
 * Author: emmanuel
 *
 * Created on January 22, 2015, 11:47 PM
 */

#ifndef EXPRESSION_H
#define	EXPRESSION_H


#include <set>

#include <iostream>


using namespace std;

class Expression {
protected:
    // conteneur de l'ensemble des expressions instanciées
    static std::set<Expression*> _pool;
public:
    Expression();
    Expression(const Expression& orig);
    //sert a evaluer l'expression
    /**
     * sert a evaluer l'expression
     * @return 
     */
    virtual double eval() const = 0;
    //libere la memoire utilise par les expressions

    /**
     * sert a liberer toutes la memoire alloues pour les expressions
     */
    static void toutLiberer();
    //affiche une expression
    /**
     * sert a afficher l'expression
     * @param os
     */
    virtual void afficher(ostream &os) const = 0;
    //cree une copie de l'expression
    /**
     * sert a cloner une expressions
     * @return 
     */
    virtual Expression* clone() const = 0;

    /**
     * sert a afficher toutes les variables existantes
     */
    static void toutAfficher();

    /**
     * sert a derive par rapport a la la variable de nom x
     * @param x
     * @return 
     */
    virtual Expression* doDerivate(string x) const = 0;

    /**
     * verifie si la variable est de type x
     * f(X) pour x.name = x
     * @param x
     * @return 
     */
    virtual bool isFunctionof(string x) const = 0;

    /**
     * sert a simplifier une expression
     * @return 
     */
    virtual Expression* simplify() = 0;
    virtual ~Expression();
    friend ostream & operator<<(ostream & os, const Expression & x);
    friend istream & operator>>(istream & is, Expression & x);

    

protected:
    /**
     * ajoute l'expression au pool d'expression
     * @param exp
     */
    static void addToPool(Expression* exp);
    
    static bool checkIfExpressionExist(Expression *);

    

};
#endif	/* EXPRESSION_H */

