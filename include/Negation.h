/* 
 * File:   Negation.h
 * Author: emmanuel
 *
 * Created on February 3, 2015, 1:51 PM
 */

#ifndef NEGATION_H
#define	NEGATION_H

#include "Expression.h"
#include <math.h>


class Negation:public Expression {
public:


    Negation(double val);
    Negation(Expression* exp);
    Negation(const Negation& orig);


    virtual void afficher(ostream& os) const;

    virtual Expression* clone() const;

    virtual ~Negation();

    virtual Expression* simplify();

    virtual bool isFunctionof(string x)const;

    virtual Expression* doDerivate(string x)const;


    virtual double eval()const;

    friend ostream & operator<<(ostream & os, const Negation & x);
    friend istream & operator>>(istream & is, Negation & x);

private:
    bool _isValueSet;
    double _val;
    Expression* _exp;
private:

};



#endif	/* NEGATION_H */

