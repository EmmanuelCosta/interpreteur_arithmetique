/* 
 * File:   Sin.h
 * Author: emmanuel
 *
 * Created on January 23, 2015, 12:13 AM
 */

#ifndef SIN_H
#define	SIN_H

#include "Expression.h"
#include <math.h>

#include "../include/Cos.h"
#include "../include/Constante.h"
#include "../include/Produit.h"
#include "../include/Variable.h"


class Sin:public Expression {
public:


    Sin(double val);
    Sin(Expression* exp);
    Sin(const Sin& orig);


    virtual void afficher(ostream& os) const;

    virtual Expression* clone() const;

    virtual ~Sin();


    virtual bool isFunctionof(string x)const;


    virtual Expression* doDerivate(string x)const;
    virtual Expression* simplify();


    virtual double eval()const;

    friend ostream & operator<<(ostream & os, const Sin & x);
    friend istream & operator>>(istream & is, Sin & x);

private:
    bool _isValueSet;
    double _val;
    Expression* _exp;
private:

};

#endif	/* SIN_H */

