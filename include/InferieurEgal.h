/* 
 * File:   InferieurEgal.h
 * Author: emmanuel
 *
 * Created on January 28, 2015, 5:51 PM
 */

#ifndef INFERIEUREGAL_H
#define	INFERIEUREGAL_H


#include "Expression_Binaire.h"
#include "../include/Constante.h"

class InferieurEgal : public Expression_Binaire {
public:

    InferieurEgal(Expression* left, Expression* right);
    InferieurEgal(const InferieurEgal& orig);
    virtual ~InferieurEgal();

    virtual Expression* clone() const;

    friend ostream & operator<<(ostream & os, const InferieurEgal & x);
    friend istream & operator>>(istream & is, InferieurEgal & x);


    virtual Expression* doDerivate(string x)const;
    virtual Expression* simplify();
    

    virtual bool isFunctionof(string x)const;


    virtual void afficher(ostream& os) const;

    virtual double eval()const;


};



#endif	/* INFERIEUREGAL_H */

