/* 
 * File:   Somme.h
 * Author: emmanuel
 *
 * Created on January 23, 2015, 12:15 AM
 */

#ifndef SOMME_H
#define	SOMME_H

#include "Expression_Binaire.h"
#include "../include/Produit.h"
#include "../include/Constante.h"
#include "../include/Variable.h"

class Somme : public Expression_Binaire {
public:

    Somme(Expression* left, Expression* right);
    Somme(const Somme& orig);
    virtual ~Somme();

    virtual Expression* clone() const;
    virtual double eval()const;
    friend ostream & operator<<(ostream & os, const Somme & x);
    friend istream & operator>>(istream & is, Somme & x);

    virtual Expression* doDerivate(string x)const;
    virtual Expression* simplify();

    virtual void afficher(ostream& os) const;

private:

};

#endif	/* SOMME_H */

