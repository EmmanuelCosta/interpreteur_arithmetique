/* 
 * File:   Exponentielle.h
 * Author: emmanuel
 *
 * Created on January 28, 2015, 5:27 PM
 */

#ifndef EXPONENTIELLE_H
#define	EXPONENTIELLE_H
#include "../include/Expression.h"
#include "../include/Produit.h"
#include "../include/Constante.h"
#include <math.h>
class Exponentielle:public Expression {
public:


    Exponentielle(double val);
    Exponentielle(Expression* exp);
    Exponentielle(const Exponentielle& orig);


    virtual void afficher(ostream& os) const;

    virtual Expression* clone() const;

    virtual ~Exponentielle();

    virtual double eval()const;

    virtual Expression* doDerivate(string x)const;
    
    virtual Expression* simplify();
    
    virtual bool isFunctionof(string x)const;


    friend ostream & operator<<(ostream & os, const Exponentielle & x);
    friend istream & operator>>(istream & is, Exponentielle & x);

private:
    bool _isValueSet;
    double _val;
    Expression* _exp;

};

#endif	/* EXPONENTIELLE_H */

