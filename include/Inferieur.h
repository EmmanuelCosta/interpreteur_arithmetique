/* 
 * File:   Inferieur.h
 * Author: emmanuel
 *
 * Created on January 28, 2015, 5:46 PM
 */

#ifndef INFERIEUR_H
#define	INFERIEUR_H
#include "../include/Expression_Binaire.h"
#include "../include/Constante.h"

class Inferieur:public Expression_Binaire {
public:
  
    Inferieur(Expression* left, Expression* right);
    Inferieur(const Inferieur& orig);
    virtual ~Inferieur();

    virtual Expression* clone() const;
    
   friend ostream & operator<<(ostream & os, const Inferieur & x);
   friend istream & operator>>(istream & is, Inferieur & x);



    virtual bool isFunctionof(string x)const;

    virtual Expression* doDerivate(string x)const;
    virtual Expression* simplify();


    virtual void afficher(ostream& os) const;

    virtual double eval()const;
    

};


#endif	/* INFERIEUR_H */

