/* 
 * File:   Puissance.h
 * Author: emmanuel
 *
 * Created on February 3, 2015, 12:02 PM
 */

#ifndef PUISSANCE_H
#define	PUISSANCE_H

#include "Expression.h"
#include <math.h>

#include "../include/Produit.h"
#include "../include/Constante.h"

class Puissance : public Expression {
public:

    Puissance(Expression* exp, int puissance);
    Puissance(const Puissance& orig);
    virtual ~Puissance();

    virtual Expression* clone() const;

    friend ostream & operator<<(ostream & os, const Puissance & x);
    friend istream & operator>>(istream & is, Puissance & x);


    virtual bool isFunctionof(string x)const;

    virtual Expression* doDerivate(string x)const;
    virtual Expression* simplify();


    virtual void afficher(ostream& os) const;

    virtual double eval()const;
    
    int getPuissance() const;
    Expression* getExpression() const;

private:
    Expression *_exp;
    int _puissance;
};
#endif	/* PUISSANCE_H */

