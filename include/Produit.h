/* 
 * File:   Produit.h
 * Author: emmanuel
 *
 * Created on January 23, 2015, 12:16 AM
 */

#ifndef PRODUIT_H
#define	PRODUIT_H

#include "Expression_Binaire.h"
#include "../include/Somme.h"
#include "../include/Puissance.h"
#include "../include/Variable.h"


class Produit:public Expression_Binaire {
public:
  
    Produit(Expression* left, Expression* right);
    Produit(const Produit& orig);
    virtual ~Produit();

    virtual Expression* clone() const;
    
   friend ostream & operator<<(ostream & os, const Produit & x);
   friend istream & operator>>(istream & is, Produit & x);


    virtual void afficher(ostream& os) const;


    virtual double eval() const;

    virtual Expression* doDerivate(string x)const;

    virtual Expression* simplify();

    


    

};

#endif	/* PRODUIT_H */

