/* 
 * File:   Monome.h
 * Author: emmanuel
 *
 * Created on February 7, 2015, 6:35 PM
 */

#ifndef MONOME_H
#define	MONOME_H

#include <iostream>
#include "Variable.h"
#include "Polynome.h"
#include "../include/Somme.h"
#include "../include/Produit.h"
#include "../include/Puissance.h"
#include "../include/Constante.h"

class Monome : public Expression {
public:
    Monome();
    Monome(Variable *var, int degre, Expression * exp);
    Monome(int degre, Expression * exp);
    Monome(const Monome& orig);
    int getDegre()const;
    Expression *getExp()const;
    Variable *getVariable() const;
    virtual ~Monome();

    virtual double eval()const;

    virtual Monome* clone() const;

    virtual Expression* doDerivate(string x)const;

    virtual bool isFunctionof(string x)const;

    virtual Expression* simplify();
    bool operator!=(const Monome&);





    Polynome operator+(const Monome&)const;
    Monome* addMonome(const Monome * m);
    Polynome* deriveToPolynome(string x)const;
    Monome operator*(const Monome&)const;

    friend ostream & operator<<(ostream & os, const Monome & x);
    void afficher(ostream& os) const;

private:
    Variable *_var;
    int _degre;
    Expression* _exp;

};

#endif	/* MONOME_H */

