/* 
 * File:   Superieur.h
 * Author: emmanuel
 *
 * Created on January 28, 2015, 5:11 PM
 */

#ifndef SUPERIEUR_H
#define	SUPERIEUR_H
#include "../include/Expression_Binaire.h"
#include "../include/Constante.h"

class Superieur:public Expression_Binaire {
public:
  
    Superieur(Expression* left, Expression* right);
    Superieur(const Superieur& orig);
    virtual ~Superieur();

    virtual Expression* clone() const;
    
   friend ostream & operator<<(ostream & os, const Superieur & x);
   friend istream & operator>>(istream & is, Superieur & x);


    virtual Expression* doDerivate(string x)const;

    virtual bool isFunctionof(string x)const;
    virtual Expression* simplify();


    virtual void afficher(ostream& os) const;

    virtual double eval()const;
    

};


#endif	/* SUPERIEUR_H */

