/* 
 * File:   SuperieurEgalEgal.h
 * Author: emmanuel
 *
 * Created on January 28, 2015, 5:47 PM
 */

#ifndef SUPERIEUREGAL_H
#define	SUPERIEUREGAL_H
#include "Expression_Binaire.h"
#include "../include/Constante.h"

class SuperieurEgal:public Expression_Binaire {
public:
  
    SuperieurEgal(Expression* left, Expression* right);
    SuperieurEgal(const SuperieurEgal& orig);
    virtual ~SuperieurEgal();

    virtual Expression* clone() const;
    
   friend ostream & operator<<(ostream & os, const SuperieurEgal & x);
   friend istream & operator>>(istream & is, SuperieurEgal & x);


    virtual bool isFunctionof(string x)const;

    virtual Expression* doDerivate(string x)const;
    virtual Expression* simplify();


    virtual void afficher(ostream& os) const;

    virtual double eval()const;
    

};


#endif	/* SUPERIEUREGAL_H */

