/* 
 * File:   Division.h
 * Author: emmanuel
 *
 * Created on January 28, 2015, 5:21 PM
 */

#ifndef DIVISION_H
#define	DIVISION_H

#include "Expression_Binaire.h"
#include "../include/Difference.h"
#include "../include/Puissance.h"
#include "../include/Produit.h"
#include "../include/Variable.h"
#include "../include/Constante.h"

class Division : public Expression_Binaire {
public:

    Division(Expression* left, Expression* right);
    Division(const Division& orig);
    virtual ~Division();

    virtual Expression* clone() const;

    friend ostream & operator<<(ostream & os, const Division & x);
    friend istream & operator>>(istream & is, Division & x);

    virtual Expression* doDerivate(string x)const;
    virtual Expression* simplify();

    virtual void afficher(ostream& os) const;

    virtual double eval()const;


};
#endif	/* DIVISION_H */

