/* 
 * File:   Affectation.h
 * Author: emmanuel
 *
 * Created on January 23, 2015, 12:11 AM
 */

#ifndef AFFECTATION_H
#define	AFFECTATION_H

#include "Expression.h"
#include "Variable.h"
#include "Constante.h"

class Affectation : public Expression {
public:
    Affectation(Variable *var,Expression *exp);
    Affectation(const Affectation& orig);
    virtual ~Affectation();

    virtual void afficher(ostream& os) const;

    virtual Expression* clone() const;

    virtual Expression* doDerivate(string x)const;

    virtual bool isFunctionof(string x)const;
    virtual Expression* simplify();


    virtual double eval()const;
    
    friend ostream & operator<<(ostream & os, const Affectation & x);
    friend istream & operator>>(istream & is, Affectation & x);



private:
    Variable *_var;
    Expression * _exp;
    bool _isAlreadyDone;
};

#endif	/* AFFECTATION_H */

