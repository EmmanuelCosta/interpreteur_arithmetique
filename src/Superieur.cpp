/* 
 * File:   Superieur.cpp
 * Author: emmanuel
 * 
 * Created on January 28, 2015, 5:11 PM
 */

#include "../include/Superieur.h"


Superieur::Superieur(Expression* left, Expression* right) : Expression_Binaire(left, right, ">") {
}

Superieur::Superieur(const Superieur& orig) : Expression_Binaire(orig) {

}

Superieur::~Superieur() {
}

double Superieur::eval()const {
    if (_left->eval() > _right->eval() ){

            return 1.0;
       }
    return 0.0;
}

Expression* Superieur::clone() const {

    return new Superieur(*this);
}

void Superieur::afficher(ostream& os) const {

    Expression_Binaire::afficher(os);
}

bool Superieur::isFunctionof(string x) const{
    return false;
}

Expression* Superieur::doDerivate(string x)const {
    return new Constante(0.0);
}



ostream & operator<<(ostream & os, const Superieur & x) {
    x.afficher(os);
    return os;
}

Expression* Superieur::simplify(){
    return new Superieur(_left->simplify(),_right->simplify());
}

