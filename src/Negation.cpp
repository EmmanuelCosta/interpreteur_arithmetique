/* 
 * File:   Negation.cpp
 * Author: emmanuel
 * 
 * Created on February 3, 2015, 1:51 PM
 */

#include "../include/Negation.h"

Negation::Negation(double val) : _val(val), _isValueSet(true) {
    addToPool(this);
}

Negation::Negation(Expression* exp) : _exp(exp), _val(0.0), _isValueSet(false) {
    addToPool(this);
}

Negation::Negation(const Negation& orig) {
    _isValueSet = orig._isValueSet;
    _val = orig._val;
    if (!_isValueSet) {
        this->_exp = orig._exp->clone();
    }
    addToPool(this);
}

Expression* Negation::clone() const {
    return new Negation(*this);
}

Negation::~Negation() {
    if (!_isValueSet) {
        delete _exp;
    }
}

Expression* Negation::doDerivate(string x) const{
    return new Negation(_exp->doDerivate(x));
}

bool Negation::isFunctionof(string x) const{
    return _exp->isFunctionof(x);
}

double Negation::eval() const{
    if (_isValueSet) {
        return -sin(_val);
    } else
        return -sin(_exp->eval());
}

ostream & operator<<(ostream & os, const Negation & x) {
    x.afficher(os);
    return os;
}

void Negation::afficher(ostream& os) const {
    os << "-";
    if (this->_isValueSet) {
        os << this->_val;
    } else
        os << *this->_exp;
    os << ")";
}

Expression* Negation::simplify(){
    return new Negation(_exp->simplify());
}