/* 
 * File:   Division.cpp
 * Author: emmanuel
 * 
 * Created on January 28, 2015, 5:11 PM
 */

#include "../include/Division.h"


Division::Division(Expression* left, Expression* right) : Expression_Binaire(left, right, "/") {
}

Division::Division(const Division& orig) : Expression_Binaire(orig) {

}

Division::~Division() {
}

double Division::eval() const{
    double diviseur = _right->eval();
    if ( diviseur == 0){

        throw string("On ne divise pas par zero");
       }
    return _left->eval()/diviseur;
}

Expression* Division::clone() const {
    return new Division(*this);
}

void Division::afficher(ostream& os) const {
    Expression_Binaire::afficher(os);
}


ostream & operator<<(ostream & os, const Division & x) {
    x.afficher(os);
    return os;
}

Expression* Division::doDerivate(string x) const{
    return new Division(new Difference(new Produit(_left->doDerivate(x),_right),new Produit(_left,_right->doDerivate(x))),new Puissance(_right,2));
}

Expression* Division::simplify(){
    Variable* isLeft = dynamic_cast < Variable *> (_left);
    Variable* isRight = dynamic_cast < Variable *> (_right);
   
    if(isLeft == 0 && isRight==0){
        return new Division(_left->simplify(), _right->simplify());
    }
    
    if(isLeft && isRight){
        if(isLeft->getName()==isRight->getName()){
            return new Constante(1.0);
        }
    }
    if(isLeft == 0){
        return new Division(_left->simplify(),_right);
    }
    
    return new Division(_left,_right->simplify());
}