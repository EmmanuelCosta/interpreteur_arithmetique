/* 
 * File:   Puissance.cpp
 * Author: emmanuel
 * 
 * Created on February 3, 2015, 12:02 PM
 */

#include "../include/Puissance.h"


Puissance::Puissance(Expression* exp, int puissance) : _exp(exp->clone()), _puissance(puissance) {

}

Puissance::Puissance(const Puissance& orig) {
    _puissance = orig._puissance;
    _exp = orig._exp->clone();
}

Puissance::~Puissance() {
    delete _exp;
}

double Puissance::eval() const{
    return pow(_exp->eval(), _puissance);
}

Expression* Puissance::clone() const {

    return new Puissance(*this);
}

void Puissance::afficher(ostream& os) const {

    os << *_exp << "^" << _puissance;
}

Expression* Puissance::getExpression() const {
    return _exp;
}

int Puissance::getPuissance() const {
    return _puissance;
}

ostream & operator<<(ostream & os, const Puissance & x) {
    x.afficher(os);
    return os;
}

bool Puissance::isFunctionof(string x) const{
    return _exp->isFunctionof(x);
}
Expression* Puissance::simplify(){
    return new Puissance(_exp->simplify(),_puissance);
}
Expression* Puissance::doDerivate(string x)const {
    return new Produit(new Produit(new Constante(_puissance), _exp->doDerivate(x)), new Puissance(_exp, _puissance - 1));
}
