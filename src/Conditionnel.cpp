/* 
 * File:   Conditionnel.cpp
 * Author: emmanuel
 * 
 * Created on January 23, 2015, 12:06 AM
 */

#include "../include/Conditionnel.h"



Conditionnel::Conditionnel() {
}

Conditionnel::Conditionnel(const Conditionnel& orig) {
    _condition=orig.getTest();
    _sValide=orig.getOutTrue();
    _sMauvaise=orig.getOutFalse();
    addToPool(this);
}

Conditionnel::Conditionnel(Expression* condition, Expression* sValide, Expression* sMauvaise): _condition(condition), _sValide(sValide), _sMauvaise(sMauvaise) {
    addToPool(this);
}


Conditionnel::~Conditionnel() {
    delete _condition;
    delete _sValide;
    delete _sMauvaise;
}

Expression* Conditionnel::clone() const {
    return new Conditionnel(_condition->clone(),_sValide->clone(),_sMauvaise->clone());
}

Expression* Conditionnel::getTest() const{
    return _condition;
}

Expression* Conditionnel::getOutTrue() const{
    return _sValide;
}

Expression* Conditionnel::getOutFalse() const{
    return _sMauvaise;
}

double Conditionnel::eval()const{
    if(_condition->eval() == 0.0){
        return _sMauvaise->eval();
    }
    return _sValide->eval();
}

void Conditionnel::afficher(ostream& os) const{
     os <<"("<< *_condition << ")?" << *_sValide << ":" << *_sMauvaise ;
}

ostream & operator<<(ostream & os, const Conditionnel & x) {
    x.afficher(os);
    return os;
}

Expression* Conditionnel::doDerivate(string x) const{
    return new Constante(0.0);
}

bool Conditionnel::isFunctionof(string x) const{
    return false;
}

Expression* Conditionnel::simplify(){
    return new Conditionnel(_condition->simplify(),_sValide->simplify(),_sMauvaise->simplify());
}
