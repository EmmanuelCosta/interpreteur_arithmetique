/* 
 * File:   Expression.cpp
 * Author: emmanuel
 * 
 * Created on January 22, 2015, 11:47 PM
 */

#include "../include/Expression.h"

#include "../include/Constante.h"
#include "../include/Variable.h"



set<Expression*> Expression::_pool;

Expression::Expression() {
}

Expression::Expression(const Expression& orig) {
}

Expression::~Expression() {
    //    cout<< "call delete of expression before " << _pool.size()<< endl;
    _pool.erase(this);
    //     cout<< "call delete of expression after " << _pool.size()<< endl;
}

void Expression::addToPool(Expression* exp) {
    _pool.insert(exp);
}

void Expression::toutLiberer() {
    if(_pool.size()<=0)
        return;
    cout << "Liberation de la memoire allouees pour l'ensemble des expressions " << endl;
    set<Expression *>::iterator beginIt(_pool.end());
    --beginIt;
    
    for (; beginIt != _pool.begin(); beginIt--) {
        if (checkIfExpressionExist(*beginIt)) {
            delete *beginIt;
        }
    }
    cout << "Liberation terminee " << endl;
    _pool.clear();
}

void Expression::toutAfficher() {
    set<Expression *>::const_iterator sit(_pool.begin());

    for (; sit != _pool.end(); sit++)
        cout << **sit << endl;

}

bool Expression::checkIfExpressionExist(Expression*exp) {
    if(exp == NULL)
        return false;
    set<Expression *>::iterator elt(_pool.find(exp));
    return elt != _pool.end();
}

ostream & operator<<(ostream & os, const Expression & x) {
    x.afficher(os);
    return os;
}