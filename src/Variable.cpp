/* 
 * File:   Variable.cpp
 * Author: emmanuel
 * 
 * Created on January 23, 2015, 12:00 AM
 */

#include "../include/Variable.h"


map<string, double> Variable::_variablePool;

Variable::Variable(string name) : _name(name) {
    insertToVariablePool(name, 0.0);
}

Variable::Variable(string name, double value) : _name(name) {

    insertToVariablePool(name, value);
}

Variable::Variable(string name, Expression* exp) : _name(name) {
    insertToVariablePool(name, exp->eval());
}

Variable::Variable(const Variable& orig) {
    _name = orig._name;
}

Variable::~Variable() {
    _variablePool.clear();
}

Variable* Variable::clone() const {
    return new Variable(*this);
}

void Variable::effacerMemoire() {
    _variablePool.clear();
}

Expression* Variable::doDerivate(string x)const {
    if (!isFunctionof(x)) {
        return new Constante(0.0);
    }
    return new Constante(1.0);
}

bool Variable::isFunctionof(string x) const {
    if (!checkIfExpressionExist(x)) {
        return false;
    }
    return x == _name;
}

void Variable::retirerExpression(string name) {
    map<string, double>::iterator it = _variablePool.find(name);
    cout << "variable mapper ( " << name << " ) trouvee. Elle va etre retirer de la map" << endl;
    //    _variablePool.erase(it);
}

void Variable::toutAfficher() {
    map<string, double>::const_iterator beginIt(_variablePool.begin());
    map<string, double>::const_iterator endIt(_variablePool.end());

    for (; beginIt != endIt; ++beginIt)
        cout << "variablePool[ " << beginIt->first << "] = " << beginIt->second << endl;
}

void Variable::insertToVariablePool(string name, double val) {
   
    if (checkIfExpressionExist(name) && val!=0)  {
        cout << "variable exist "<< name <<endl;
           map<string, double>::iterator it = _variablePool.find(name);
        _variablePool.erase(it);
      
       _variablePool.find(name)->second = val;
    }else{
          _variablePool.insert(std::pair<string, double>(name, val));
    }
  
}

bool Variable::checkIfExpressionExist(string name) {
    map<string, double>::const_iterator it = _variablePool.find(name);
    return it != _variablePool.end();
}

double Variable::eval()const {

    if (checkIfExpressionExist(_name)) {
        double exp = _variablePool.find(_name)->second;
        return exp;
    }
    return 0.0;

}

string Variable::getName()const {
    return _name;
}

void Variable::set(double val) {
    _variablePool.find(_name)->second = val;
}

void Variable::set(Expression * exp) {
    _variablePool.find(_name)->second = exp->eval();
}

void Variable::afficher(ostream& os) const {
    os << _name;
}

ostream & operator<<(ostream & os, const Variable & x) {
    x.afficher(os);
    return os;
}

Expression* Variable::simplify() {
    return this;
}
