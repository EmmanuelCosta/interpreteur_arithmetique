/* 
 * File:   Pour.cpp
 * Author: sybille
 * 
 * Created on 3 février 2015, 19:17
 */



#include "../include/Pour.h"


Pour::Pour() {
}

Pour::Pour(const Pour& orig) {
    _actionFinDeBoucle=orig.getActionFinBoucle();
    _calcul=orig.getCalcul();
    _condtion=orig.getCondition();
    _init=orig.getInit();
//    addToPool(this);
    
}

Pour::~Pour() {
}

Pour::Pour(Expression* init, Expression* condition, Expression* actionFinDeBoucle, Expression* calcul): _init(init),_condtion(condition), _actionFinDeBoucle(actionFinDeBoucle),_calcul(calcul){
    addToPool(this);
}

double Pour::eval()const{
    double res;
    _init->eval();
    while(_condtion->eval()){
        res =_calcul->eval();
        _actionFinDeBoucle->eval();
    }
    return res;
}
 
Expression* Pour::clone() const{
    new Pour(_init->clone(),_condtion->clone(),_actionFinDeBoucle->clone(),_calcul->clone());
}

void Pour::afficher(ostream& os) const{
    os <<"for("<< *_init << ";" << *_condtion << ";" << *_actionFinDeBoucle<<"){"<< endl;
    os << *_calcul << ";" << endl;
    os << "}"<< endl;
}

Expression* Pour::getActionFinBoucle() const{
    return _actionFinDeBoucle;
}

Expression* Pour::getCalcul() const{
    return _calcul;
}

Expression* Pour::getInit() const{
    return _init;
}


Expression* Pour::getCondition() const{
    return _condtion;
}

ostream & operator<<(ostream & os, const Pour & x) {
    x.afficher(os);
    return os;
}

Expression* Pour::doDerivate(string x) const{
    return new Constante(0.0);
}

bool Pour::isFunctionof(string x)const {
    return false;
}

Expression* Pour::simplify(){
    _actionFinDeBoucle=_actionFinDeBoucle->simplify();
    _calcul=_calcul->simplify();
    _init=_init->simplify();
    return new Pour(_init->simplify(),_condtion->simplify(),_actionFinDeBoucle->simplify(),_calcul->simplify());
}
