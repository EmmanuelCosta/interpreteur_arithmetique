/* 
 * File:   Conditionnel.cpp
 * Author: emmanuel
 * 
 * Created on January 23, 2015, 12:06 AM
 */

#include "../include/IfThenElse.h"



IfThenElse::IfThenElse() {
}

IfThenElse::IfThenElse(const IfThenElse& orig) {
    _condition=orig.getTest();
    _sValide=orig.getOutTrue();
    _sMauvaise=orig.getOutFalse();
//    addToPool(this);
}

IfThenElse::IfThenElse(Expression* condition, Expression* sValide, Expression* sMauvaise): _condition(condition), _sValide(sValide), _sMauvaise(sMauvaise) {
    addToPool(this);
}


IfThenElse::~IfThenElse() {
    delete _condition;
    delete _sValide;
    delete _sMauvaise;
}

Expression* IfThenElse::clone() const {
    return new IfThenElse(*this);
}

Expression* IfThenElse::getTest() const{
    return _condition;
}

Expression* IfThenElse::getOutTrue() const{
    return _sValide;
}

Expression* IfThenElse::getOutFalse() const{
    return _sMauvaise;
}

double IfThenElse::eval()const{
    if(_condition->eval() == 0.0){
        return _sMauvaise->eval();
    }
    return _sValide->eval();
}

Expression* IfThenElse::doDerivate(string x)const {
    return new Constante(0.0);
}

bool IfThenElse::isFunctionof(string x)const {
    return false;
}



void IfThenElse::afficher(ostream& os) const{
     os <<"If"<< *_condition << "Then" << *_sValide << "Else" << *_sMauvaise ;
}
Expression* IfThenElse::simplify(){
    return new IfThenElse(_condition->simplify(),_sValide->simplify(),_sMauvaise->simplify());
}

ostream & operator<<(ostream & os, const IfThenElse & x) {
    x.afficher(os);
    return os;
}
