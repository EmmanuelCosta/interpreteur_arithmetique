/* 
 * File:   Constante.cpp
 * Author: emmanuel
 * 
 * Created on January 22, 2015, 11:56 PM
 */

#include "../include/Constante.h"

Constante::Constante(double val) : _val(val), _isValueSet(true) {
    addToPool(this);
}

Constante::Constante(Expression* exp) : _exp(exp->clone()), _val(0.0), _isValueSet(false) {
    addToPool(this);
}

Constante::Constante(const Constante& orig) {
    _isValueSet = orig._isValueSet;
    _val = orig._val;
    if (!_isValueSet) {
        this->_exp = orig._exp->clone();
    }
//    addToPool(this);
}

Expression* Constante::clone() const {
    return new Constante(*this);
}

Constante::~Constante() {
    if (!_isValueSet) {
        delete _exp;
    }

}

double Constante::eval()const {
    if (_isValueSet) {
        return _val;
    } else
        return _exp->eval();
}

ostream & operator<<(ostream & os, const Constante & x) {
    x.afficher(os);
    return os;
}

void Constante::afficher(ostream& os) const {
    if (this->_isValueSet) {
        os << this->_val;
    } else
        os << *this->_exp;
}

Expression* Constante::doDerivate(string x)const {
    return new Constante(0.0);
}

bool Constante::isFunctionof(string x) const{
    return false;
}

Expression* Constante::simplify(){
        return new Constante(this);
}