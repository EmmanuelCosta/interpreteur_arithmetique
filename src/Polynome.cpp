/* 
 * File:   Polynome.cpp
 * Author: emmanuel
 * 
 * Created on February 7, 2015, 1:27 PM
 */

#include "../include/Polynome.h"
#include "../include/Monome.h"

Polynome::Polynome(string x) : _variable(x), _degreMax(0) {
    addToPool(this);
}

string Polynome::getVariableName() const {
    return _variable;
}

void Polynome::setVariableName(string x) {
    _variable = x;
}

void Polynome::addMonome(Monome* monome) {
    int deg = monome->getDegre();
    set<int>::iterator it = _available.find(deg);

    //si il existe deja un monome du meme deg
    // jadditionne
    if (it != _available.end()) {
        map<int, Monome *>::iterator polyIterator = _polynome.find(deg);

        Monome * internal = polyIterator->second;
        Monome* result = internal->addMonome(monome);
        //        set<int>::const_iterator itBegin = result._available.begin();
        //je recupere les elements de chaque monome
        // et je reitere sur addMonome pour chacun d'eux
        _polynome.erase(polyIterator);
        _polynome.insert(std::pair<int, Monome*>(deg, result));
        // si il n'existe pas
        // j'ajoute
    } else {
        set<int>::const_iterator itBegin = _available.begin();


        _available.insert(deg);
        _polynome.insert(std::pair<int, Monome*>(deg, monome));
        if (monome->getDegre() > _degreMax) {
            _degreMax = monome->getDegre();
        }

    }
}

double Polynome::eval()const {
    Expression *exp = doHorner();
    return exp->eval();
}

Polynome::Polynome(const Polynome& orig) {
    _variable = orig._variable;
    _degreMax = orig._degreMax;
    set<int>::const_iterator it2 = orig._available.begin();
    for (; it2 != orig._available.end(); ++it2) {
        _available.insert(*it2);
        map<int, Monome *>::const_iterator it3 = orig._polynome.find(*it2);
        Monome * m2 = it3->second;
        _polynome[*it2] = m2->clone();
    }
}

Polynome::~Polynome() {
    set<int>::iterator it = _available.begin();
    for (; it != _available.end(); ++it) {
        map<int, Monome *>::iterator polyResultIterator = _polynome.find(*it);
        Monome * m = polyResultIterator->second;
        _polynome.erase(polyResultIterator);
        delete m;
    }
    //    _available.clear();
    //    _polynome.clear();

}

void Polynome::afficher(ostream& os) const {
    set<int>::const_iterator itBegin = _available.begin();

    for (; itBegin != _available.end(); ++itBegin) {
        int pos = *itBegin;
        map<int, Monome *>::const_iterator polyResultIterator = _polynome.find(pos);
        Monome * elt = polyResultIterator->second;

        os << *elt;
        if (pos < _degreMax) {

            //                os << "eval a donné " <<  elt->getExp()->eval() << endl;
            os << "+";

        }

    }
}

ostream & operator<<(ostream & os, const Polynome & x) {
    x.afficher(os);
    return os;
}

Polynome Polynome::operator+(const Polynome&p)const {

    if (_variable != p._variable) {
        throw string("Ne supporte que la somme entre deux polynome ayant la meme variable"
                "ex: P(x)+V(x) est ok mais pas O(x)+V(n) sauf si x=n");
    }

    Polynome polynome(_variable);
    set<int>::const_iterator itBegin = _available.begin();

    for (; itBegin != _available.end(); itBegin++) {
        int pos = *itBegin;
        map<int, Monome *>::const_iterator polyResultIterator = _polynome.find(pos);
        Monome * firstElt = polyResultIterator->second;

        set<int>::const_iterator secondItBegin = p._available.find(pos);

        if (secondItBegin != p._available.end()) {

            map<int, Monome *>::const_iterator secondPolyResultIterator = p._polynome.find(pos);
            Monome * secondElt = secondPolyResultIterator->second;

            Somme* mysum = new Somme(firstElt->getExp()->clone(), secondElt->getExp()->clone());

            Monome * result = new Monome(new Variable(_variable), firstElt->getDegre(), new Constante(mysum->eval()));

            polynome.addMonome(result);
        } else {
            polynome.addMonome(firstElt->clone());
        }

    }
    if (_degreMax < p._degreMax) {
        for (int index = _degreMax + 1; index <= p._degreMax; index++) {
            set<int>::const_iterator secondItBegin = p._available.find(index);
            if (secondItBegin != p._available.end()) {
                map<int, Monome *>::const_iterator secondPolyResultIterator = p._polynome.find(index);
                Monome * secondElt = secondPolyResultIterator->second;
                polynome.addMonome(secondElt->clone());
            }
        }
    }
    return polynome;
}

Polynome Polynome::operator-(const Polynome&p) const {

    if (_variable != p._variable) {
        throw string("Ne supporte que la somme entre deux polynome ayant la meme variable"
                "ex: P(x)+V(x) est ok mais pas O(x)+V(n) sauf si x=n");
    }

    Polynome polynome(_variable);
    set<int>::const_iterator itBegin = _available.begin();

    for (; itBegin != _available.end(); itBegin++) {
        int pos = *itBegin;
        map<int, Monome *>::const_iterator polyResultIterator = _polynome.find(pos);
        Monome * firstElt = polyResultIterator->second;

        set<int>::const_iterator secondItBegin = p._available.find(pos);

        if (secondItBegin != p._available.end()) {

            map<int, Monome *>::const_iterator secondPolyResultIterator = p._polynome.find(pos);
            Monome * secondElt = secondPolyResultIterator->second;

            Difference* mysum = new Difference(firstElt->getExp()->clone(), secondElt->getExp()->clone());

            Monome * result = new Monome(new Variable(_variable), firstElt->getDegre(), new Constante(mysum->eval()));

            polynome.addMonome(result);
        } else {
            polynome.addMonome(firstElt->clone());
        }

    }
    if (_degreMax < p._degreMax) {
        for (int index = _degreMax + 1; index <= p._degreMax; index++) {
            set<int>::const_iterator secondItBegin = p._available.find(index);
            if (secondItBegin != p._available.end()) {
                map<int, Monome *>::const_iterator secondPolyResultIterator = p._polynome.find(index);
                Monome * secondElt = secondPolyResultIterator->second;
                polynome.addMonome(secondElt->clone());
            }
        }
    }
    return polynome;
}

Polynome Polynome::operator*(const Polynome&p)const {
    if (_variable != p._variable) {
        throw string("Ne supporte que la somme entre deux polynome ayant la meme variable"
                "ex: P(x)+V(x) est ok mais pas O(x)+V(n) sauf si x=n");
    }

    Polynome polynome(_variable);
    set<int>::iterator itBegin = _available.begin();



    for (; itBegin != _available.end(); itBegin++) {
        map<int, Monome *>::const_iterator polyResultIterator = _polynome.find(*itBegin);
        Monome * m1 = polyResultIterator->second;
        set<int>::iterator secondItBegin = p._available.begin();
        for (; secondItBegin != p._available.end(); secondItBegin++) {
            map<int, Monome *>::const_iterator secondPolyResultIterator = p._polynome.find(*secondItBegin);
            Monome * m2 = secondPolyResultIterator->second;
            Monome result = *m1 * *m2;
            polynome.addMonome(new Monome(result.getVariable(), result.getDegre(), result.getExp()->clone()));
        }
    }
    return polynome;
}

Polynome Polynome::operator=(const Polynome&p) {
    if (&p != this) {
        _variable = p._variable;
        _degreMax = p._degreMax;
        set<int>::iterator it = _available.begin();
        for (; it != _available.end(); ++it) {
            map<int, Monome *>::iterator polyResultIterator = _polynome.find(*it);
            Monome * m = polyResultIterator->second;
            delete m;
        }
        _polynome.clear();
        _available.clear();

        set<int>::const_iterator it2 = p._available.begin();
        for (; it2 != p._available.end(); ++it2) {
            _available.insert(*it2);
            map<int, Monome *>::const_iterator it3 = p._polynome.find(*it2);
            Monome * m2 = it3->second;

            _polynome[*it2] = m2->clone();
        }


    }
    return Polynome(p);
}

Polynome Polynome::operator+=(const Polynome& p) {
    *this = *this+p;
    return *this;
}

Polynome Polynome::operator-=(const Polynome& p) {
    *this = *this-p;
    return *this;
}

Polynome Polynome::operator*=(const Polynome& p) {
    *this = *this*p;
    return *this;
}

bool Polynome::operator==(const Polynome& p) {
    Polynome v(p);
    double a = this->eval();
    double b = v.eval();
    if (_degreMax != v._degreMax) {
        return false;
    }


    for (int i = 0; i < _degreMax; i++) {
        set<int>::iterator it = _available.find(i);
        set<int>::iterator it2 = v._available.find(i);

        if ((it == _available.end())&&(it2 != v._available.end())) {
            return false;
        } else

            if ((it != _available.end())&&(it2 == v._available.end())) {
            return false;
        } else {
            map<int, Monome *>::iterator polyResultIterator = _polynome.find(*it);
            map<int, Monome *>::iterator polyResultIterator2 = v._polynome.find(*it);
            Monome * m1 = polyResultIterator->second;
            Monome * m2 = polyResultIterator->second;
            if (m1 != m2) {

                return false;
            }
        }
    }
    return true;


}

bool Polynome::operator!=(const Polynome& p) {
    return !(*this == p);
}

Polynome* Polynome::clone() const {

    return new Polynome(*this);
}

Expression* Polynome::doDerivate(string x)const {
    Polynome *p = new Polynome(_variable);
    set<int>::const_iterator itBegin = _available.begin();

    for (; itBegin != _available.end(); itBegin++) {
         map<int, Monome *>::const_iterator polyResultIterator = _polynome.find(*itBegin);
        Monome * firstElt = polyResultIterator->second;
        Polynome * poly = firstElt->deriveToPolynome(x);
        *p+=*poly;
    }
    return p;
}

bool Polynome::isFunctionof(string x) const {

    return x == _variable;
}

Expression* Polynome::simplify() {

    return this;
}

/**
 * Cette methode va decomposer le polynome en facteur en utyilisant la methode de horner
 * L'algorithme ici est inspirer de celui explique sur : http://uel.unisciel.fr/mathematiques/polynomes1/polynomes1_ch03/co/apprendre_ch3_1_09.html
 * 
 * @return 
 */
Expression* Polynome::doHorner() const {

    return doHornerFrom(0);
}

Expression* Polynome::doHornerFrom(int i) const {
    Constante * val = NULL;
    if (i > _degreMax) {
        return new Constante(0.0);
    } else {

        set<int>::iterator isAvailable = _available.find(i);
        if (isAvailable == _available.end()) {
            val = new Constante(0.0);
        } else {
            map<int, Monome *>::const_iterator polyIterator = _polynome.find(i);
            Monome * m = polyIterator->second;
            val = new Constante(m->getExp());
        }

        return new Somme(val, new Produit(new Variable(_variable), doHornerFrom(i + 1)));
    }
}
