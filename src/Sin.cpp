/* 
 * File:   Sin.cpp
 * Author: emmanuel
 * 
 * Created on January 23, 2015, 12:13 AM
 */

#include "../include/Sin.h"


Sin::Sin(double val) : _val(val), _isValueSet(true) {
    addToPool(this);
}

Sin::Sin(Expression* exp) : _exp(exp->clone()), _val(0.0), _isValueSet(false) {
    addToPool(this);
}

Sin::Sin(const Sin& orig) {
    _isValueSet = orig._isValueSet;
    _val = orig._val;
    if (!_isValueSet) {
        this->_exp = orig._exp->clone();
    }
//    addToPool(this);
}

Expression* Sin::clone() const {
    return new Sin(*this);
}

Sin::~Sin() {
    if (!_isValueSet) {
        delete _exp;
    }
}

bool Sin::isFunctionof(string x) const{
    return _exp->isFunctionof(x);
}

Expression* Sin::doDerivate(string x) const{
    if (_isValueSet) {
        return new Constante(0.0);
    } else {
        //_exp est fonction de 
        if (_exp->isFunctionof(x)) {
            return new Produit(_exp->doDerivate(x), new Cos(_exp));
        } else {
            return new Constante(0.0);
        }
    }
}

double Sin::eval() const{
    if (_isValueSet) {
        return sin(_val);
    } else
        return sin(_exp->eval());
}

ostream & operator<<(ostream & os, const Sin & x) {
    x.afficher(os);
    return os;
}

void Sin::afficher(ostream& os) const {
    os << "sin(";
    if (this->_isValueSet) {
        os << this->_val;
    } else
        os << *this->_exp;
    os << ")";
}

Expression* Sin::simplify(){
    return new Sin(_exp->simplify());
}