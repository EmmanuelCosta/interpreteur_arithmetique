#include <stdio.h>
#include <stdlib.h>
#include <set>
#include <iostream>
#include <fstream>
#include <math.h>
#include "../include/Constante.h"
#include "../include/Somme.h"
#include "../include/Cos.h"
#include "../include/Sin.h"
#include "../include/Produit.h"
#include "../include/Superieur.h"
#include "../include/Division.h"
#include "../include/InferieurEgal.h"
#include "../include/Conditionnel.h"
#include "../include/Variable.h"
#include "../include/Affectation.h"
#include "../include/Puissance.h"
#include "../include/IfThenElse.h"
#include "../include/Pour.h"
#include "../include/Bloc.h"
#include "../include/Polynome.h"
#include "../include/Monome.h"
#include "../include/Exponentielle.h"



using namespace std;

void testConstante() {
    // c = 5
    Expression * c = new Constante(5);
    cout << "c = " << *c << endl;
    Expression * cbis = c->clone();
    cout << "clone de c = " << *cbis << endl;
    delete c;
    delete cbis;
}

void testConstante2() {
    Variable v = Variable("x", 18.0);
    // c = 5
    Expression * c = new Constante(&v);
    cout << "c doit renvoyer 18 " << endl;
    cout << "c->eval = " << c->eval() << endl;
    delete c;
}

void testCosinus() {
    // c = cos(PI/3)
    Cos * c = new Cos(new Constante(M_PI / 3.0));
    cout << *c << " = " << c->eval() << endl;
    delete c;
    cout << "destruction automatique des variables locales allouees sur la PILE: ICI AUCUNE " << endl;
}

void testExponentielle() {
    // c = 5
    Expression * c = new Exponentielle(5);
    cout << "c = " << *c << endl;
    Expression * cbis = c->clone();
    cout << "clone de c = " << *cbis << endl;
    cout << "c->eval = " << c->eval() << endl;
    delete c;
    delete cbis;
}

void testBinaire() {
    // s = 1 + 2 * sin(PI/6)
    Somme * s = new Somme(new Constante(1.0), new Produit(new Constante(2.0), new Sin(new Constante(M_PI / 6.0))));
    cout << "s : " << *s << " = " << s->eval() << endl;

    Expression * sbis = s->clone();
    cout << "clone de s : " << *sbis << " = " << sbis->eval() << endl;

    // s > 1

    //    Superieur comp(s->clone(), new Constante(1.8));
    //    cout << comp << " = " << (bool)comp.eval() << endl;

    delete s;
    delete sbis;
    cout << "destruction automatique des variables locales allouees sur la PILE: ICI COMP" << endl;
}

void testBinaire2() {
    // s = 1 - 2 / sin(PI/6)
    Difference * d = new Difference(new Constante(1.0), new Division(new Constante(2.0), new Sin(new Constante(M_PI / 6.0))));
    cout << "d : " << *d << " = " << d->eval() << endl;

    Expression * dbis = d->clone();
    cout << "clone de d : " << *dbis << " = " << dbis->eval() << endl;



    delete d;
    delete dbis;
    cout << "destruction automatique des variables locales allouees sur la PILE: ICI COMP" << endl;
}

void testVariable1() {
    // x = 3
    Variable x("x", 3.0);
    // y = 0
    Variable y("y");
    cout << x << " = " << x.eval() << endl;
    cout << y << " = " << y.eval() << endl;

    // exp = 1 + 2 * x
    Expression * exp = new Somme(new Constante(1.0), new Produit(new Constante(2.0), &x));
    // a = (y <- exp)
    Affectation * a = new Affectation(new Variable("y"), exp->clone());
    cout << *a << " = " << a->eval() << endl;
    cout << y << " = " << y.eval() << endl;

    Variable::effacerMemoire();
    delete exp; // OK car il existe un clone
    delete a;
    cout << "destruction automatique des variables locales allouees sur la PILE: ICI X et Y" << endl;
}

void testVariable2() {
    // x = PI/3
    Variable * x = new Variable("x", M_PI / 3.0);
    cout << *x << " = " << x->eval() << endl;
    // x = x + (-10)
    x->set(x->eval() + -10);
    cout << *x << " = " << x->eval() << endl;

    Variable::effacerMemoire();
}

void testConditionnel() {
    // x = 0
    Variable * x = new Variable("x", 8);

    // expr = (x > 0) ? x + 10 : x * 10
    Conditionnel * expr =
            new Conditionnel(new Superieur(x, new Constante(0.0)),
            new Somme(x, new Constante(10.0)),
            new Produit(x, new Constante(10.0)));

    cout << *x << " = " << x->eval() << endl;
    cout << *expr << "\n EVAL ternaire : " << expr->eval() << endl;
    ;
    cout << *x << " = " << x->eval() << endl;
    // x = expr OU ENCORE x = (x > 0) ? x + 10 : x * 10
    x->set(expr->eval());
    cout << *x << " = " << x->eval() << endl;
    cout << *expr << "\n EVAL ternaire : " << expr->eval() << endl;
    cout << *x << " = " << x->eval() << endl;
    Variable::effacerMemoire();
}

void testIfThenElse() {
    // x = 0
    Variable * x = new Variable("x", 8);

    // expr = if (x > 0) x = x + 10  else x = x * 10
    IfThenElse * expr =
            new IfThenElse(new Superieur(x, new Constante(0.0)),
            new Affectation(x, new Somme(x, new Constante(10.0))),
            new Affectation(x, new Produit(x, new Constante(10.0))));
    cout << *x << " = " << x->eval() << endl;
    cout << *expr << "\n EVAL if then else : " << expr->eval() << endl;
    ;
    cout << *x << " = " << x->eval() << endl;

    Variable::effacerMemoire();
}

void testBloc() {
    // x = 5
    Variable * x = new Variable("x", 5);
    // bloc = { x = x + 9 }
    Bloc * bloc = new Bloc("b1", new Affectation(x, new Somme(x, new Constante(9))));

    //  bloc =  {
    //              x = x + 9
    //              if (x > 20) x = x + 10  else x = x * 10
    //          }
    bloc->add(new IfThenElse(new Superieur(x, new Constante(20.0)),
            new Affectation(x, new Somme(x, new Constante(10.0))),
            new Affectation(x, new Produit(x, new Constante(10.0)))));
    // y = 3
    Variable * y = new Variable("y", 3);
    //  bloc =  {
    //              x = x + 9
    //              if (x > 20) x = x + 10  else x = x * 10
    //              y = x + -100
    //          }
    bloc->add(new Affectation(y, new Somme(x, new Constante(-100))));

    cout << *x << " = " << x->eval() << endl;
    cout << *y << " = " << y->eval() << endl;
    cout << *bloc << "\n EVAL bloc : " << bloc->eval() << endl;
    cout << *x << " = " << x->eval() << endl;
    cout << *y << " = " << y->eval() << endl;

    Variable::effacerMemoire();
}

// Calcul d'une factorielle

void testPour1() {
    // x = 3
    Variable * x = new Variable("x", 3.0);
    cout << *x << " = " << x->eval() << endl;

    // i = 0
    Variable * i = new Variable("i");
    cout << *i << " = " << i->eval() << endl;

    // init = (i = 1)
    Expression * init = new Affectation(i, new Constante(1));
    // condition = (x > i - 1)
    Expression * condition = new Superieur(x, new Somme(i, new Constante(-1)));
    // actionFinDeBoucle = (i = i + 1)
    Expression * actionFinDeBoucle = new Affectation(i, new Somme(i, new Constante(1)));
    // calcul = (res = res * i)
    Variable * res = new Variable("res", 1);
    Expression * calcul(new Affectation(res, new Produit(res, i)));

    // pour = for (i = 1; x > i; i = i + 1) res = res * i
    Expression * pour = new Pour(init, condition, actionFinDeBoucle, calcul);
    cout << *pour << "\n EVAL pour : " << pour->eval() << endl;
    cout << x->eval() << "!" << " = " << res->eval() << endl;

    // x = 5
    x->set(5.0);
    cout << *x << " = " << x->eval() << endl;
    // condition = (x > i - 1)
    condition = new Superieur(x, new Somme(i, new Constante(-1)));
    // pour = for (i = 1; x > i; i = i + 1) res = res * i
    res->set(1);
    pour = new Pour(init, condition, actionFinDeBoucle, calcul);
    cout << *pour << "\n EVAL pour : " << pour->eval() << endl;
    cout << x->eval() << "!" << " = " << res->eval() << endl;

    Variable::effacerMemoire();
}

// boucle avec plusieurs instructions (un bloc)

void testPour2() {
    // x = 3
    Variable * x = new Variable("x", 3.0);
    cout << *x << " = " << x->eval() << endl;

    // i = 0
    Variable * i = new Variable("i");
    cout << *i << " = " << i->eval() << endl;
    // init = (i = 1)
    Expression * init = new Affectation(i, new Constante(1.0));
    // condition = (4 > i)
    Expression * condition = new Superieur(new Constante(4), i);
    // actionFinDeBoucle = (i = i + 1)
    Expression * actionFinDeBoucle = new Affectation(i, new Somme(i, new Constante(1)));
    // calcul =   {
    //              x = x + 9
    //              if (x > 20) x = x + 10  else x = x * 10
    //              y = 3
    //              y = x + -100
    //          }

    // bloc =   {
    //              x = x + 9
    //          }
    Bloc * bloc = new Bloc("b1", new Affectation(x, new Somme(x, new Constante(9))));

    // bloc =   {
    //              x = x + 9
    //              if (x > 20) x = x + 10  else x = x * 10
    //              y = 3
    //              y = x + -100
    //          }
    bloc->add(new IfThenElse(new Superieur(x, new Constante(20.0)),
            new Affectation(x, new Somme(x, new Constante(10.0))),
            new Affectation(x, new Produit(x, new Constante(10.0)))));
    Variable * y = new Variable("y", 5);
    bloc->add(new Affectation(y, new Somme(x, new Constante(-100))));


    // calcul =   {
    //              x = x + 9
    //              if (x > 20) x = x + 10  else x = x * 10
    //              y = 3
    //              y = x + -100
    //
    Expression * calcul(bloc);

    // pour = for (i = 1; 4 > i; i = i + 1) {
    //              x = x + 9
    //              if (x > 20) x = x + 10  else x = x * 10
    //              y = 3
    //              y = x + -100
    //          }
    Expression * pour = new Pour(init, condition, actionFinDeBoucle, calcul);
    cout << *pour << "\n EVAL pour : " << pour->eval() << endl;
    cout << *x << " = " << x->eval() << endl;
    cout << *y << " = " << y->eval() << endl;
}

void testPour3() {
    // x = 3
    Variable * x = new Variable("x", 3.0);
    cout << *x << " = " << x->eval() << endl;
    // i = 0
    Variable * i = new Variable("i");
    cout << *i << " = " << i->eval() << endl;

    // init1 = (i = 1)
    Expression * init1 = new Affectation(i, new Constante(1.0));
    // condition1 = (4 > i)
    Expression * condition1 = new Superieur(new Constante(4), i);
    // actionFinDeBoucle1 = (i = i + 1)
    Expression * actionFinDeBoucle1 = new Affectation(i, new Somme(i, new Constante(1)));

    // y = 5
    Variable * y = new Variable("y", 5.0);
    cout << *y << " = " << y->eval() << endl;
    // j = 0
    Variable * j = new Variable("j");
    cout << *j << " = " << j->eval() << endl;
    // init2 = (j = 1)
    Expression * init2 = new Affectation(j, new Constante(1.0));
    // condition2 = (3 > j)
    Expression * condition2 = new Superieur(new Constante(3), j);
    // actionFinDeBoucle2 = (j = j + 1)
    Expression * actionFinDeBoucle2 = new Affectation(j, new Somme(j, new Constante(1)));


    // bloc =   {
    //              x = x + 9
    //          }
    Bloc * bloc = new Bloc("b1", new Affectation(x, new Somme(x, new Constante(9))));

    // bloc =   {
    //              x = x + 9
    //              if (x > 20) x = x + 10  else x = x * 10
    //              y = 3
    //              y = x + -100
    //          }
    bloc->add(new IfThenElse(new Superieur(x, new Constante(20.0)),
            new Affectation(x, new Somme(x, new Constante(10.0))),
            new Affectation(x, new Produit(x, new Constante(10.0)))));
    bloc->add(new Affectation(y, new Somme(x, new Constante(-100))));


    // calcul =   {
    //              x = x + 9
    //              if (x > 20) x = x + 10  else x = x * 10
    //              y = 3
    //              y = x + -100
    //          }
    Expression * calcul(bloc);


    // pour = for (i = 1; 4 > i; i = i + 1) {
    //      pour = for (j = 1; 2 > j; j = j + 1) {
    //              x = x + 9
    //              if (x > 20) x = x + 10  else x = x * 10
    //              y = 3
    //              y = x + -100
    //          }
    // }
    Expression * pour2 = new Pour(init2, condition2, actionFinDeBoucle2, calcul);
    Expression * pour1 = new Pour(init1, condition1, actionFinDeBoucle1, pour2);
    cout << *pour1 << "\n EVAL pour1 : " << pour1->eval() << endl;
    cout << *x << " = " << x->eval() << endl;
    cout << *y << " = " << y->eval() << endl;
}

void verifBoucle2() {
    double x = 3;
    double y = 5;
    int i = 0;
    for (i = 1; 4 > i; i = i + 1) {
        x = x + 9;
        if (x > 20)
            x = x + 10;
        else
            x = x * 10;
        y = 3;
        y = x + -100;
    }

    cout << " x = " << x << endl;
    cout << " y = " << y << endl;
}

void verifBoucle3() {
    double x = 3;
    double y = 5;
    int i = 0;
    int j = 0;
    for (i = 1; 4 > i; i = i + 1) {
        for (j = 1; 3 > j; j = j + 1) {
            x = x + 9;
            if (x > 20)
                x = x + 10;
            else
                x = x * 10;
            y = 3;
            y = x + -100;
        }
    }

    cout << " x = " << x << endl;
    cout << " y = " << y << endl;
}

void testPuissance() {
    //(5*(2+8))^2
    Expression* exp = new Puissance(new Produit(new Constante(5), new Somme(new Constante(2), new Constante(8))), 2);
    cout << *exp << " = " << exp-> eval() << endl;
    cout << " FIN " << endl;
}

void testDerivation() {
    Variable *var = new Variable("b", 50.0);

    Expression* somme = new Somme(var, new Puissance(var, 3));
    Sin* sinus = new Sin(new Somme(var, new Constante(2)));

    //b+b^3)*sin(b+2) = -((1+3*b^2))*sin(b+2)+((b+b^3))*(1+0)*cos(b+2)
    Expression* prod = new Produit(somme, sinus);
    Expression * derived = prod->doDerivate("b");
    cout << *prod << "' = " << *derived << endl;
    Variable::effacerMemoire();
}

void testDerivation2() {
    Variable *var = new Variable("b", 50.0);
    Variable *var2 = new Variable("v", 50.0);
    Cos * c = new Cos(var);

    cout << *c << " =1= " << c->eval() << endl;
    Expression * exp = c->doDerivate("b");
    cout << *exp << " =2= " << exp->eval() << endl;

    Somme * s1 = new Somme(var, new Constante(2.0));
    cout << "somme = " << *s1 << " = " << s1->eval() << endl;
    Expression *exp2 = s1->doDerivate("b");
    cout << "derive = " << *exp2 << " = " << exp2->eval() << endl;

    Expression* s = new Sin(new Somme(var2, new Constante(2.0)));
    //    
    cout << *s << " ===> " << s->eval() << endl;
    Expression * sderive = s->doDerivate("v");
    cout << *sderive << " <===> " << sderive->eval() << endl;

    Cos *c3 = new Cos(new Puissance(var, 2));
    cout << *c3 << " == " << c3->eval() << endl;
    Expression *c3derive = c3->doDerivate("b");
    cout << *c3derive << " == " << c3derive->eval() << endl;


}

void testSimplification() {
    Variable *var = new Variable("b", 50.0);

    Somme * s1 = new Somme(new Constante(2.0), var);
    cout << "s1 = " << *s1 << endl;
    Expression *exp = s1->simplify();
    cout << "s1->simplify() = " << *exp << endl;

    Somme * s2 = new Somme(new Sin(var), new Sin(var));
    cout << "s2 = " << *s2 << endl;
    Expression *exp2 = s2->simplify();
    cout << "s2->simplify()" << *exp2 << endl;

    s2 = new Somme(var, var);
    Somme * s3 = new Somme(var, s2);
    cout << "s3 = " << *s3 << endl;
    Expression *exp3 = s3->simplify();
    cout << "s3->simplify()" << *exp3 << endl;

    Produit* p = new Produit(new Constante(10.0), new Somme(var, new Produit(var, new Produit(var, var))));
    cout << "p = " << *p << endl;
    Expression *exp4 = p->simplify();
    cout << "p->simplify()" << *exp4 << endl;
    Variable::effacerMemoire();

}

void testPolynome() {

    //-5y^0
    Monome * k = new Monome(new Variable("y", 10), 0, new Constante(-5.0));
    //7y^2
    Monome * l = new Monome(new Variable("y"), 2, new Constante(7.0));


    cout << " k = " << *k << endl;
    cout << " l =  " << *l << endl;
    //-5y^0 + 7y^2
    Polynome pn = *k + *l;
    cout << "pn =(  " << *k << " + " << *l << " ) = " << pn << endl;
    //-15y^0+7y^2
    Polynome pm = (*k + *k)+(*k + *l);
    cout << "pm = " << *k << "+" << *k << "+" << *l << "+" << *k << ") = " << pm << endl;

    //-5y^0 + 7y^2-17y^5
    cout << "pn.addMonome(" << "-17y^5) = ";
    pn.addMonome(new Monome(new Variable("y"), 5, new Constante(-17.0)));
    cout << pn << endl;

    ;

    //(-5y^0 + 7y^2-17y^5)*(15y^0+7y^2) = 75y^0-140y^2+49y^4+255y^5-119y^7

    cout << " p3 = (" << pn << ")*(" << pm << ") = ";
    Polynome p3 = pn * pm;
    Polynome* p4 = new Polynome("y");
    //p4 = p3
    *p4 = p3;
    Expression* p5 = p4->clone();
    //p5 = clone de p4
    cout << p3 << endl;
    cout << "p4 =p3=" << *p4 << endl;
    cout << "p5 =p4->clone() =  " << *p5 << endl;

    Variable * var = new Variable("y");
    cout << " y = " << var->eval() << endl;
    cout << "pn* =pm =  " << pn << "*=" << pm;
    pn *= pm;
    cout << " =  " << pn << endl;
    //test d' égalité
    cout << "pn == pn= " << (pn == pn) << endl;
    cout << "horner on pn = " << pn << "=" << *(pn.doHorner()) << endl;
    cout << "pm = " << pm << " = " << pm.eval() << endl;
    Variable::effacerMemoire();
}

void testHorner() {
    //-5y^0
    Monome * k = new Monome(new Variable("y", 2), 0, new Constante(-5.0));
    //7y^2
    Monome * l = new Monome(new Variable("y"), 2, new Constante(7.0));


    cout << " k = " << *k << endl;
    cout << " l =  " << *l << endl;
    //-5y^0 + 7y^2
    Polynome pn = *k + *l;
    cout << "pn =(  " << *k << " + " << *l << " ) = " << pn << endl;
    //-15y^0+7y^2
    Polynome pm = (*k + *k)+(*k + *l);
    cout << "pm = " << *k << "+" << *k << "+" << *l << "+" << *k << ") = " << pm << endl;

    //-5y^0 + 7y^2-17y^5
    cout << "pn.addMonome(" << "-17y^5) = ";
    pn.addMonome(new Monome(new Variable("y"), 5, new Constante(-17.0)));
    cout << pn << endl;

    ;

    //(-5y^0 + 7y^2-17y^5)*(15y^0+7y^2) = 75y^0-140y^2+49y^4+255y^5-119y^7

    cout << " p3 = (" << pn << ")*(" << pm << ") = ";
    Polynome p3 = pn * pm;
    cout << " p3 = " << p3 << endl;
    cout << "horner sur p3 = " << *(p3.doHorner()) << endl;
    cout << "resultat: p3 =  " << p3.eval() << endl;
    cout << "p3.derive = " << *(p3.doDerivate("y")) << endl;
    Variable::effacerMemoire();
}

void testNimporteQuoi() {

    // x = 3
    Variable * x = new Variable("x", 3.0);
    Monome * k = new Monome(x, 0, new Constante(-5.0));
    //7y^2
    Monome * l = new Monome(x, 2, new Constante(7.0));
    //-5y^0 + 7y^2
    Polynome pn = *k + *l;

    Expression* e = new Produit(new Somme(new Exponentielle(5), new Produit(new Sin(45), new Constante(18))), &pn);
    cout << "e = " << *e << endl;
    cout << "e = " << e->eval() << endl;


    cout << *x << " = " << x->eval() << endl;
    Bloc * bloc = new Bloc("b1", new Affectation(x, new Somme(x, e)));
    // i = 0
    Variable * i = new Variable("i");
    cout << *i << " = " << i->eval() << endl;
    // init = (i = 1)
    Expression * init = new Affectation(i, new Constante(1.0));
    Expression * condition = new Superieur(new Constante(4), i);
    Expression * actionFinDeBoucle = new Affectation(i, new Somme(i, new Constante(1)));
    Expression *pour = new Pour(init, condition, actionFinDeBoucle, bloc);
    cout << *pour << "\n EVAL pour : " << pour->eval() << endl;

    cout << *x << " = " << x->eval() << endl;
    Variable::effacerMemoire();
}

void testDerivationPolynome() {
    //-5y^0
    Monome * k = new Monome(new Variable("y", 2), 0, new Constante(-5.0));
    //7y^2
    Monome * l = new Monome(new Variable("y"), 2, new Constante(7.0));


    cout << " k = " << *k << endl;
    cout << " l =  " << *l << endl;
    //-5y^0 + 7y^2
    Polynome pn = *k + *l;
    cout << "pn =(  " << *k << " + " << *l << " ) = " << pn << endl;
    //-15y^0+7y^2
    Polynome pm = (*k + *k)+(*k + *l);
    cout << "pm = " << *k << "+" << *k << "+" << *l << "+" << *k << ") = " << pm << endl;

    //-5y^0 + 7y^2-17y^5
    cout << "pn.addMonome(" << "-17y^5) = ";
    pn.addMonome(new Monome(new Variable("y"), 5, new Constante(-17.0)));
    cout << pn << endl;


    cout << "pn.derive = " << *(pn.doDerivate("y")) << endl;
    Variable::effacerMemoire();
}

int main() {
    int choix = -1;



    do {
        cout << " 0 : constante" << endl;
        cout << " 1 : cosinus" << endl;
        cout << " 2 : binaire" << endl;
        cout << " 3 : variable1" << endl;
        cout << " 4 : variable2" << endl;
        cout << " 5 : conditionnel" << endl;
        cout << " 6 : if then else" << endl;
        cout << " 7 : bloc" << endl;
        cout << " 8 : boucle avec une seule expression" << endl;
        cout << " 9 : boucle avec bloc d'expressions" << endl;
        cout << " 10 : boucles imbriqu�es" << endl;
        cout << " 12 : Puissance" << endl;
        cout << " 13 : Derivation" << endl;
        cout << " 14 : Simplification" << endl;
        cout << " 15 : Polynome" << endl;
        cout << " 16 : constante2" << endl;
        cout << " 17 : Derivation2" << endl;
        cout << " 18 : binaire2" << endl;
        cout << " 19 : exponentielle" << endl;
        cout << " 20 : N'importe quoi" << endl;
        cout << " 21 : Test Horner " << endl;
        cout << " 22 : Test derivée d'un polynome " << endl;
        cout << " 11 : tous les tests" << endl;
        cout << " 666 : quitter" << endl;
        cout << "choix : ";
        cin >> choix;
        switch (choix) {
            case 0:
                testConstante();
                break;
            case 1:
                testCosinus();
                break;
            case 2:
                testBinaire();
                break;
            case 3:
                testVariable1();
                break;
            case 4:
                testVariable2();
                break;
            case 5:
                testConditionnel();
                break;
            case 6:
                testIfThenElse();
                break;
            case 7:
                testBloc();
                break;
            case 8:
                testPour1();
                break;
            case 9:
                testPour2();
                cout << "VERIFICATION" << endl;
                verifBoucle2();
                break;
            case 10:
                testPour3();
                cout << "VERIFICATION" << endl;
                verifBoucle3();
                break;
            case 12: testPuissance();
                break;
            case 13: testDerivation();
                break;
            case 14: testSimplification();
                break;
            case 15: testPolynome();
                break;
            case 16: testConstante2();
                break;
            case 17: testDerivation2();
                break;
            case 18: testBinaire2();
                break;
            case 19: testExponentielle();
                break;
            case 20: testNimporteQuoi();
                break;
            case 21: testHorner();
                break;
            case 22: testDerivationPolynome();
                break;
            case 11:
                testConstante();
                testConstante2();
                testCosinus();
                testBinaire();
                testBinaire2();
                testVariable1();
                testVariable2();
                testConditionnel();
                testIfThenElse();
                testBloc();
                testPour1();
                testPour2();
                cout << "VERIFICATION" << endl;
                verifBoucle2();
                testPour3();
                cout << "VERIFICATION" << endl;
                verifBoucle3();
                testDerivation();
                testDerivation2();
                testSimplification();
                testPolynome();
                testExponentielle();
                testNimporteQuoi();
                testHorner();
                testDerivationPolynome();

                break;

            default:
                cout << "cas inconnu!" << endl;
                break;
        }
    } while (choix != 666);

    Expression::toutLiberer();

    return 0;
}


