/* 
 * File:   Monome.cpp
 * Author: emmanuel
 * 
 * Created on February 7, 2015, 6:35 PM
 */

#include "../include/Monome.h"

Monome::Monome() {
}

Monome::Monome(Variable* var, int degre, Expression* exp) : _var(var), _degre(degre), _exp(exp->clone()) {
    addToPool(this);
}

Monome::Monome(int degre, Expression* exp) : _var(new Variable("UNDIFINED")), _degre(degre), _exp(exp->clone()) {
    addToPool(this);
}

int Monome::getDegre() const {
    return _degre;
}

Expression* Monome::getExp() const {
    return _exp;
}

Variable* Monome::getVariable() const {
    return _var;
}

Polynome Monome::operator+(const Monome& b) const {
    Polynome p(_var->getName());
    Monome *tmp = new Monome(b.getVariable(), b.getDegre(), b.getExp()->clone());
    if ((_var->getName() != b.getVariable()->getName()) || (_degre != b.getDegre())) {

        p.addMonome(this->clone());
        p.addMonome(tmp);
    } else {
        Somme *s = new Somme(this->getExp()->clone(), b.getExp()->clone());

        p.addMonome(new Monome(b.getVariable(), b._degre, new Constante(s->eval())));
    }
    return p;
}

Monome Monome::operator*(const Monome& b)const {
    Produit * p = new Produit(_exp, b._exp);
    int deg = b._degre + _degre;
    return Monome(new Variable(_var->getName()), deg, new Constante(p->eval()));

}

Monome* Monome::addMonome(const Monome *m) {
    Somme * p = new Somme(_exp, m->_exp);

    return new Monome(new Variable(_var->getName()), _degre, new Constante(p->eval()));

}

double Monome::eval() const {
    Produit *p = new Produit(_exp, new Puissance(_var, _degre));
    return p->eval();
}

Monome::Monome(const Monome& orig) {
    _degre = orig._degre;
    _exp = orig._exp->clone();
    _var = orig._var->clone();
}

Monome::~Monome() {
    delete _exp;
}

void Monome::afficher(ostream& os) const {
    os << *_exp << *_var << "^" << _degre;
}

ostream & operator<<(ostream & os, const Monome & x) {
    x.afficher(os);
    return os;
}

Monome* Monome::clone() const {
    return new Monome(*this);
}

Expression* Monome::doDerivate(string x) const {
    Produit * toDerivate = new Produit(_exp, new Puissance(_var, _degre));
    return toDerivate->doDerivate(x);
}

Polynome* Monome::deriveToPolynome(string x)const {
    Produit * toDerivate = new Produit(_exp, new Puissance(_var, _degre));
//    cout << endl << " le produit est = " << *toDerivate << endl;
    Somme *s = dynamic_cast<Somme*> (toDerivate->doDerivate(x));
//    cout << "la derive est " << *s << endl;
    //gauche
    Produit *pL = dynamic_cast<Produit*> (s->getLeftOperande());
    Puissance *puL = dynamic_cast<Puissance*> (pL->getRightOperande());
    int degre = puL->getPuissance();
    Monome* l = new Monome(new Variable(x), degre, pL->getLeftOperande());
//    cout << "derive to monome " << *l << endl;
    //droite

    Produit *pR = dynamic_cast<Produit*> (s->getRightOperande());
    Produit *pR2 = dynamic_cast<Produit*> (pR->getRightOperande());
    Produit * tokeep = new Produit(pR->getLeftOperande(),pR2->getLeftOperande());
//    cout << "tokeep =" << *tokeep << endl;
     Puissance *puR = dynamic_cast<Puissance*> (pR2->getRightOperande());
    
     degre = puR->getPuissance();
    Monome* m = new Monome(new Variable(x), degre, tokeep);
//    cout << "derive to monome " << *m<< endl;
    Polynome *po = new Polynome(x);
    Polynome tmp = *l+*m;
    
    *po+=tmp;
    return po;


}

bool Monome::isFunctionof(string x)const {
    return _var->isFunctionof(x);
}

Expression* Monome::simplify() {
    return this;
}

bool Monome::operator!=(const Monome&p) {
    Monome v(p);
    if (this->_degre != v.getDegre()) {
        return false;
    }
    if (_var->getName() != v.getVariable()->getName()) {
        return false;
    }
    if (_exp->eval() != v.eval()) {
        return false;
    }
    return true;
}
