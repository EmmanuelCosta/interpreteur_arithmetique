/* 
 * File:   Inferieur.cpp
 * Author: emmanuel
 * 
 * Created on January 28, 2015, 5:46 PM
 */

#include "../include/Inferieur.h"


Inferieur::Inferieur(Expression* left, Expression* right) : Expression_Binaire(left, right, "<") {
}

Inferieur::Inferieur(const Inferieur& orig) : Expression_Binaire(orig) {

}

Inferieur::~Inferieur() {
}

double Inferieur::eval() const{
    if (_left->eval() < _right->eval() ){

            return 1.0;
       }
    return 0.0;
}

Expression* Inferieur::clone() const {

    return new Inferieur(*this);
}

void Inferieur::afficher(ostream& os) const {

    Expression_Binaire::afficher(os);
}


ostream & operator<<(ostream & os, const Inferieur & x) {
    x.afficher(os);
    return os;
}

Expression* Inferieur::doDerivate(string x) const{
    return new Constante(0.0);
}

bool Inferieur::isFunctionof(string x) const{
    return false;
}

Expression* Inferieur::simplify(){
    return new Inferieur(_left->simplify(),_right->simplify());
}

