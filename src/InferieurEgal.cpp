/* 
 * File:   InferieurEgal.cpp
 * Author: emmanuel
 * 
 * Created on January 28, 2015, 5:51 PM
 */

#include "../include/InferieurEgal.h"


InferieurEgal::InferieurEgal(Expression* left, Expression* right) : Expression_Binaire(left, right, "<=") {
}

InferieurEgal::InferieurEgal(const InferieurEgal& orig) : Expression_Binaire(orig) {

}

InferieurEgal::~InferieurEgal() {
}

double InferieurEgal::eval() const{
    if (_left->eval() <= _right->eval() ){

            return 1.0;
       }
    return 0.0;
}

Expression* InferieurEgal::clone() const {

    return new InferieurEgal(*this);
}

void InferieurEgal::afficher(ostream& os) const {

    Expression_Binaire::afficher(os);
}

Expression* InferieurEgal::doDerivate(string x)const {
    return new Constante(0.0);
}

bool InferieurEgal::isFunctionof(string x)const {
    return false;
}

Expression* InferieurEgal::simplify(){
    return new InferieurEgal(_left->simplify(),_right->simplify());
}

ostream & operator<<(ostream & os, const InferieurEgal & x) {
    x.afficher(os);
    return os;
}

