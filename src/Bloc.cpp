/* 
 * File:   Bloc.cpp
 * Author: emmanuel
 * 
 * Created on January 23, 2015, 12:08 AM
 */

#include "../include/Bloc.h"


Bloc::Bloc() {
}

Bloc::Bloc(const Bloc& orig) {
    _bloc = orig.getList();
}

Bloc::Bloc(string name, Expression* exp) {
    _bloc.push_back(exp);
    _name = name;
}

Bloc::~Bloc() {
}

Expression* Bloc::clone() const{
    
}

list<Expression*> Bloc::getList() const {
    return _bloc;
}

void Bloc::add(Expression* exp){
    _bloc.push_back(exp);
}

double Bloc::eval()const {

    list<Expression*>::const_iterator iter;
    iter = this->_bloc.begin();

    while (iter != this->_bloc.end()) {
        (*iter)->eval();
        ++iter;
    }
    return 0.0;
}


void Bloc::afficherTout(){
    list<Expression*>::const_iterator beginIt(_bloc.begin());
    list<Expression*>::const_iterator endIt(_bloc.end());
    
    for (; beginIt != endIt; ++beginIt)
        cout <<  (*beginIt) <<  endl;
}
void Bloc::afficher(ostream& os)const{
   os << _name;
}

Expression* Bloc::doDerivate(string x) const{
    return new Constante(0.0);
}

bool Bloc::isFunctionof(string x)const {
    return false;
}

Expression* Bloc::simplify(){
    list<Expression*>::iterator iter;
    iter = this->_bloc.begin();

    while (iter != this->_bloc.end()) {
        (*iter)=(*iter)->simplify();
        ++iter;
    }
    return this;
}

ostream & operator<<(ostream & os, const Bloc & x) {
    x.afficher(os);
    return os;
}