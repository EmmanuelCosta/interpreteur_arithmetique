/* 
 * File:   Expression_Binaire.cpp
 * Author: emmanuel
 * 
 * Created on January 23, 2015, 12:03 AM
 */

#include "../include/Expression_Binaire.h"

Expression_Binaire::Expression_Binaire(Expression* left, Expression* right, string operateur) : _left(left->clone()), _right(right->clone()), _operateur(operateur) {
    addToPool(this);
}

Expression_Binaire::Expression_Binaire(const Expression_Binaire& orig) {
    _left = orig.getLeftOperande()->clone();
    _right = orig.getRightOperande()->clone();
    _operateur = orig.getOperateur();
//    addToPool(this);
}

Expression_Binaire::~Expression_Binaire() {
    delete _left;
    delete _right;
}

Expression* Expression_Binaire::getLeftOperande() const {
    return _left;
}

Expression* Expression_Binaire::getRightOperande() const {
    return _right;
}

bool Expression_Binaire::isFunctionof(string x)const {
    return _left->isFunctionof(x) || _right->isFunctionof(x);
}

void Expression_Binaire::afficher(ostream &os) const {
    os << "(" << *_left << " " << getOperateur() << " " << *_right << ")";
}

string Expression_Binaire::getOperateur() const {
    return _operateur;
}








