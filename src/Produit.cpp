/* 
 * File:   Produit.cpp
 * Author: emmanuel
 * 
 * Created on January 23, 2015, 12:16 AM
 */

#include "../include/Produit.h"


Produit::Produit(Expression* left, Expression* right) : Expression_Binaire(left, right,"*") {
}

Produit::Produit(const Produit& orig) : Expression_Binaire(orig) {

}


Produit::~Produit() {
}

double Produit::eval() const {
   return  _left->eval() * _right->eval();
}

Expression* Produit::clone() const {
    return new Produit(*this);
}

void Produit::afficher(ostream& os) const {
    Expression_Binaire::afficher(os);
}


ostream & operator<<(ostream & os, const Produit & x) {
    x.afficher(os);
    return os;
}

Expression* Produit::doDerivate(string x) const{
    return new Somme(new Produit(_left->doDerivate(x),_right),new Produit(_left,_right->doDerivate(x)));
}

Expression* Produit::simplify(){
    Variable* isLeft = dynamic_cast < Variable *> (_left);
    Variable* isRight = dynamic_cast < Variable *> (_right);
   
    if(isLeft == 0 && isRight==0){
        return new Produit(_left->simplify(), _right->simplify());
    }
    
    if(isLeft && isRight ){
        if(isLeft->getName()==isRight->getName()){
            return new Puissance(_left,2);
        }
    }
    if(isLeft == 0){
        return new Produit(_left->simplify(),_right);
    }
    
    return new Produit(_left,_right->simplify());
}