/* 
 * File:   Exponentielle.cpp
 * Author: emmanuel
 * 
 * Created on January 28, 2015, 5:27 PM
 */

#include "../include/Exponentielle.h"


Exponentielle::Exponentielle(double val) : _val(val), _isValueSet(true) {
    addToPool(this);
}

Exponentielle::Exponentielle(Expression* exp) : _exp(exp->clone()), _val(0.0), _isValueSet(false) {
    addToPool(this);
}

Exponentielle::Exponentielle(const Exponentielle& orig) {
    _isValueSet = orig._isValueSet;
    _val = orig._val;
    if (!_isValueSet) {
        this->_exp = orig._exp->clone();
    }
//    addToPool(this);
}

Expression* Exponentielle::clone() const {
    return new Exponentielle(*this);
}

Exponentielle::~Exponentielle() {
    if (!_isValueSet) {
        delete _exp;
    }
}

double Exponentielle::eval() const{
    if (_isValueSet) {
        return exp(_val);
    } else
        return exp(_exp->eval());
}

Expression* Exponentielle::doDerivate(string x)const {
    if (_isValueSet) {
        return new Constante(0.0);
    } else {
        //_exp est fonction de 
        if (_exp->isFunctionof(x)) {
            return new Produit(_exp->doDerivate(x), new Exponentielle(_exp));
        } else {
            return new Constante(0.0);
        }
    }
}

bool Exponentielle::isFunctionof(string x)const {
    return _exp->isFunctionof(x);
}


ostream & operator<<(ostream & os, const Exponentielle & x) {
    x.afficher(os);
    return os;
}

void Exponentielle::afficher(ostream& os) const {
    os << "exp(";
    if (this->_isValueSet) {
        os << this->_val;
    } else
        os << *this->_exp;

    os << ")";
}

Expression* Exponentielle::simplify(){
    return new Exponentielle(_exp->simplify());
}
        