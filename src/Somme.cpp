/* 
 * File:   Somme.cpp
 * Author: emmanuel
 * 
 * Created on January 23, 2015, 12:15 AM
 */

#include "../include/Somme.h"


Somme::Somme(Expression* left, Expression* right):Expression_Binaire(left,right,"+") {   
}


Somme::Somme(const Somme& orig):Expression_Binaire(orig) {   
}


Somme::~Somme() {
}

Expression* Somme::clone() const {
    return new Somme(*this);
}

double Somme::eval()const  {
   return  _left->eval() + _right->eval();
}

Expression* Somme::doDerivate(string x)const {
    return new Somme(_left->doDerivate(x),_right->doDerivate(x));
}

void Somme::afficher(ostream &os) const {
    Expression_Binaire::afficher(os) ;    
}

ostream & operator<<(ostream & os, const Somme & x) {
    x.afficher(os);
    return os;
}

Expression* Somme::simplify(){
    Variable* isLeft = dynamic_cast < Variable *> (_left);
    Variable* isRight = dynamic_cast < Variable *> (_right);
    
    if(isLeft==0 && isRight==0){
        return new Somme(_left->simplify(), _right->simplify());
    }
    
    if(isLeft && isRight){
        if(isLeft->getName()==isRight->getName()){
            return new Produit(_left,new Constante(2.0));
        }
    }
    if(isLeft==0){
        return new Somme(_left->simplify(),_right);
    }

    return new Somme(_left,_right->simplify());
}