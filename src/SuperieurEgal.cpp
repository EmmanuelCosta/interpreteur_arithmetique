/* 
 * File:   SuperieurEgalEgal.cpp
 * Author: emmanuel
 * 
 * Created on January 28, 2015, 5:47 PM
 */

#include "../include/SuperieurEgal.h"


SuperieurEgal::SuperieurEgal(Expression* left, Expression* right) : Expression_Binaire(left, right, ">=") {
}

SuperieurEgal::SuperieurEgal(const SuperieurEgal& orig) : Expression_Binaire(orig) {

}

SuperieurEgal::~SuperieurEgal() {
}

double SuperieurEgal::eval()const {
    if (_left->eval() >= _right->eval()) {

        return 1.0;
    }
    return 0.0;
}

Expression* SuperieurEgal::clone() const {

    return new SuperieurEgal(*this);
}

void SuperieurEgal::afficher(ostream& os) const {

    Expression_Binaire::afficher(os);
}

ostream & operator<<(ostream & os, const SuperieurEgal & x) {
    x.afficher(os);
    return os;
}

Expression* SuperieurEgal::doDerivate(string x) const{
    return new Constante(0.0);
}

bool SuperieurEgal::isFunctionof(string x) const{
    return false;
}

Expression* SuperieurEgal::simplify(){
    return new SuperieurEgal(_left->simplify(),_right->simplify());
}

