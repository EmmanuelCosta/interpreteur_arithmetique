/* 
 * File:   Cos.cpp
 * Author: emmanuel
 * 
 * Created on January 23, 2015, 12:13 AM
 */

#include "../include/Cos.h"


Cos::Cos(double val) : _val(val), _isValueSet(true) {
    addToPool(this);
}

Cos::Cos(Expression* exp) : _exp(exp->clone()), _val(0.0), _isValueSet(false) {
    addToPool(this);
}

Cos::Cos(const Cos& orig) {
    _isValueSet = orig._isValueSet;
    _val = orig._val;
    if (!_isValueSet) {
        this->_exp = orig._exp->clone();
    }
//    addToPool(this);
}

Expression* Cos::clone() const {
    return new Cos(*this);
}

Cos::~Cos() {
    if (!_isValueSet) {
        delete _exp;
    }
}

double Cos::eval() const{
    if (_isValueSet) {
        return cos(_val);
    } else
        return cos(_exp->eval());
}

Expression* Cos::doDerivate(string x)const {
    if (_isValueSet) {
        return new Constante(0.0);
    } else {
        //_exp est fonction de 
        if (_exp->isFunctionof(x)) {
            return new Negation(new Produit(_exp->doDerivate(x),new Sin(_exp)));
        } else {
            return new Constante(0.0);
        }
    }
}

bool Cos::isFunctionof(string x)const {
    bool b = _exp->isFunctionof(x);
    return b;
}

Expression* Cos::simplify(){
    return new Cos(_exp->simplify());
}

ostream & operator<<(ostream & os, const Cos & x) {
    x.afficher(os);
    return os;
}

void Cos::afficher(ostream& os) const {
    os << "cos(";
    if (this->_isValueSet) {
        os << this->_val;
    } else
        os << *this->_exp;
    os << ")";
}