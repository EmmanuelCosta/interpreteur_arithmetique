/* 
 * File:   Affectation.cpp
 * Author: emmanuel
 * 
 * Created on January 23, 2015, 12:11 AM
 */

#include "../include/Affectation.h"


Affectation::Affectation(Variable* var, Expression* exp) : _var(var->clone()), _exp(exp->clone()),_isAlreadyDone(false) {

}

Affectation::Affectation(const Affectation& orig) {
    _var = orig._var->clone();
    _exp = orig._exp->clone();
    _isAlreadyDone = orig._isAlreadyDone;
}

Affectation::~Affectation() {
}

double Affectation::eval()const {
   // if (!_isAlreadyDone) {
        _var->set(_exp);
//        _isAlreadyDone=true;
 //   }
    return _var->eval();
}

Expression* Affectation::doDerivate(string x) const{
    return new Constante(0.0);
}

bool Affectation::isFunctionof(string x)const {
    return false;
}

Expression* Affectation::clone() const {
    return new Affectation(*this);
}

void Affectation::afficher(ostream& os) const {
    os << *_exp;
}

Expression* Affectation::simplify(){
    return new Affectation(_var,_exp->simplify());
}
ostream & operator<<(ostream & os, const Affectation & x) {
    x.afficher(os);
    return os;
}

