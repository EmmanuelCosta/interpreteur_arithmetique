/* 
 * File:   Soustraction.cpp
 * Author: emmanuel
 * 
 * Created on January 28, 2015, 5:38 PM
 */




#include "../include/Difference.h"


Difference::Difference(Expression* left, Expression* right) : Expression_Binaire(left, right, "-") {
}

Difference::Difference(const Difference& orig) : Expression_Binaire(orig) {

}

Difference::~Difference() {
}

double Difference::eval() const{
    return _left->eval()-_right->eval();
}

Expression* Difference::clone() const {

    return new Difference(*this);
}

void Difference::afficher(ostream& os) const {

    Expression_Binaire::afficher(os);
}


ostream & operator<<(ostream & os, const Difference & x) {
    x.afficher(os);
    return os;
}

Expression* Difference::doDerivate(string x) const{
    return new Difference(_left->doDerivate(x),_right->doDerivate(x));
}

Expression* Difference::simplify(){
    Variable* isLeft = dynamic_cast < Variable *> (_left);
    Variable* isRight = dynamic_cast < Variable *> (_right);
   
    if(isLeft == 0 && isRight==0){
        return new Difference(_left->simplify(), _right->simplify());
    }
    
    if(isLeft  && isRight ){
        if(isLeft->getName()==isRight->getName()){
            return new Constante(0.0);
        }
    }
    if(isLeft ==0){
        return new Difference(_left->simplify(),_right);
    }
    
    return new Difference(_left,_right->simplify());
    
}


